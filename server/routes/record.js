const express = require("express");
const recordRoutes = express.Router();
const dbo = require("../db/conn");
const ObjectId = require("mongodb").ObjectId;
const bodyParser = require('body-parser');
const {response} = require("express");
let urlencodedParser = bodyParser.urlencoded({ extended: false });

recordRoutes.route("/products").post(urlencodedParser,function(req, response){
    let db_connect = dbo.getDb("products");
    db_connect.collection("products").findOne({name: `${req.body.name}`}, function(err, result) {
        if (err) throw err;
        if (result) {
            response.send("This product already exists")
        } else {
            let newProduct = {...req.body};
            db_connect.collection("products").insertOne(newProduct, function (err, res) {
                if (err) throw err;
                response.send(res);
            });
        }
    });

});

recordRoutes.route("/products").get(function(req, res) {
    let db_connect = dbo.getDb("products");
    let filter = {};
    let sort = {};
    let limit = 0;
    if (req.query.filterBy && req.query.filterValue) {
        filter[req.query.filterBy] = req.query.filterValue;
    }
    if (req.query.sortBy) {
        sort = {[req.query.sortBy] : 1};
    }
    if (req.query.limit){
        limit = parseInt(req.query.limit);
    }
    db_connect.collection("products").find(filter).sort(sort).limit(limit).toArray(function(err, result) {
        if (err) throw err;
        res.json(result);
    });
});

recordRoutes.route("/products/:id").delete(function (req, res) {
    let db_connect = dbo.getDb("products");
    let myQuery = {_id: ObjectId(req.params.id)};
    db_connect.collection("products").findOne(myQuery, function(err, result) {
        if (err) throw err;
        if(result){
            db_connect.collection("products").deleteOne(myQuery, function(err, obj){
                if (err) throw err;
                console.log("1 document deleted");
                res.json(obj);
            });
        }
        else {
            res.send("This product doesn't exist");
        }
    });
})

recordRoutes.route("/products/:id").put(urlencodedParser,function(req, response){
    let db_connect = dbo.getDb("products");
    let myQuery = {_id: ObjectId(req.params.id)};
    let newValues = {}
    if(req.body.name){
        newValues.name = req.body.name
    }
    if(req.body.price){
        newValues.price = req.body.price
    }
    if(req.body.description){
        newValues.description =  req.body.description
    }
    if(req.body.amount){
        newValues.ilosc = req.body.amount
    }
    if(req.body.unit){
        newValues.unit = req.body.unit
    }

    db_connect.collection("products").updateOne(myQuery, { $set: newValues}, function(err, res){
        if (err) throw err;
        console.log("1 document updated successfully");
        response.json(res);
    });
});


recordRoutes.route("/products/report").get(function(req, res) {
    let db_connect = dbo.getDb("products");

    db_connect.collection("products").aggregate([
        {
            $addFields: {
                price: { $toDouble: "$price" },
                amount: { $toInt: "$amount" }
            }
        },
        {
            $project: {
                _id: 0,
                name: 1,
                amount: 1,
                price: 1,
                totalPrice: { $multiply: ["$amount", "$price"] }
            }
        }
    ]).toArray(function(err, result) {
        if (err) {
            console.log(err);
            res.send("There was an error during report generation");
            return;
        }

        res.json(result);
    });
});

module.exports = recordRoutes;
